# README #

This is a quick Arduino project for a Kub Kar track timer.

### Wiring ###

* (D2 - SW - GND) Reset switch (held closed when car gate is up)
* (D3 - SW - GND) Car #1 sensor
* (D4 - SW - GND) Car #2 sensor
* (D5 - SW - GND) Car #3 sensor
* (D6 - SW - GND) Car #4 sensor
* (D7 - SW - GND) Car #... sensor
* (A4) SDA on LCD (YwRobot LCM1602 IIC V1)
* (A5) SDL on LCD



